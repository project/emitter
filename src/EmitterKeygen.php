<?php

declare(strict_types=1);

namespace Drupal\emitter;

use Drupal\Core\Cache\CacheBackendInterface;
use Psr\Log\LoggerInterface;

/**
 * Abstract base class for a emitter keygen.
 */
abstract class EmitterKeygen {

  /**
   * Construct new EmitterKeygen.
   *
   * @param EmitterInterface $emitter
   *   The emitter client.
   * @param Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend.
   * @param Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param int $ttl
   *   TTL of the generated key.
   * @param int $regenerateThreshold
   *   Time in seconds where the key can be generated before actually expiring.
   */
  public function __construct(
    private EmitterInterface $emitter,
    private CacheBackendInterface $cache,
    private LoggerInterface $logger,
    private int $ttl,
    private int $regenerateThreshold,
  ) {}

  /**
   * Get key for given parameters.
   *
   * If a key for the given parameters has already been generated,
   * it will be loaded from cache.
   *
   * @param string $channel
   *   The channel.
   * @param string $permissions
   *   The key permissions.
   *
   * @return string
   *   The channel key.
   */
  protected function getKeyFromCache(string $channel, string $permissions): string {
    $hash = $this->createHash($channel, $permissions);

    // Load from cache.
    if ($item = $this->cache->get($hash)) {
      return $item->data;
    }

    // Key is not cached, generate it.
    $key = $this->generateKey($channel, $permissions);

    $this->cache->set($hash, $key, time() + $this->ttl - $this->regenerateThreshold);

    return $key;
  }

  /**
   * Generate new channel key.
   *
   * @param string $channel
   *   The channel.
   * @param string $permissions
   *   The key permissions.
   *
   * @return string
   *   The channel key.
   */
  protected function generateKey(string $channel, string $permissions): string {
    $this->logger->debug('Generate key for channel @channel with permissions @perms', [
      '@channel' => $channel,
      '@perms' => $permissions,
    ]);

    return $this->emitter->keygenGlobal(
      $channel,
      $permissions,
      $this->ttl,
    );
  }

  /**
   * Generate unique hash for given channel and permissions.
   *
   * @param string $channel
   *   The channel.
   * @param string $permissions
   *   The key permissions.
   *
   * @return string
   *   The hash.
   */
  protected function createHash(string $channel, string $permissions): string {
    return md5(
      sprintf(
        '%s:%s:%d',
        $channel,
        $permissions,
        $this->ttl,
      ),
    );
  }

}
