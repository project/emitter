<?php

declare(strict_types=1);

namespace Drupal\emitter;

use Wunderwerk\EmitterSDK\Emitter as EmitterBase;

/**
 * Wrapper class for the Emitter SDK.
 *
 * Provides additional methods for convenience.
 */
class Emitter extends EmitterBase implements EmitterInterface {

  /**
   * Global master key.
   */
  protected ?string $masterKey;

  /**
   * {@inheritdoc}
   */
  public function setMasterKey(string $masterKey): self {
    $this->masterKey = $masterKey;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function keygenGlobal(string $channel, string $type, int $ttl): string {
    if (!$this->masterKey) {
      throw new \Exception('No master key set.');
    }

    return parent::keygen($this->masterKey, $channel, $type, $ttl);
  }

}
