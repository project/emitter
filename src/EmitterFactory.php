<?php

declare(strict_types=1);

namespace Drupal\emitter;

use Drupal\Core\Site\Settings;

/**
 * Factory to create a new Emitter instance.
 */
class EmitterFactory {

  /**
   * Creates a new emitter instance.
   *
   * @return EmitterInterface
   *   The created instance.
   */
  public static function createEmitter(): EmitterInterface {
    $settings = self::getSettings();

    $emitter = new Emitter();

    // Connect to instance.
    $emitter->connect($settings['host'], $settings['port'], $settings['username']);

    // Set master key.
    if ($settings['master_key']) {
      $emitter->setMasterKey($settings['master_key']);
    }

    return $emitter;
  }

  /**
   * Get settings for emitter connection.
   *
   * @return array
   *   The settings.
   */
  protected static function getSettings(): array {
    $settings = Settings::get('emitter.connection');

    if (!isset($settings['host'])) {
      throw new \Exception('No hostname set for emitter connection.');
    }

    if (!isset($settings['port'])) {
      throw new \Exception('No port set for emitter connection.');
    }

    $settings += [
      'master_key' => NULL,
      'username' => NULL,
    ];

    return $settings;
  }

}
