<?php

declare(strict_types=1);

namespace Drupal\emitter;

use Wunderwerk\EmitterSDK\EmitterInterface as EmitterInterfaceBase;

/**
 * Interface for the Emitter class.
 */
interface EmitterInterface extends EmitterInterfaceBase {

  /**
   * Sets the master key on the instance.
   *
   * This key is used to generate other keys.
   *
   * @param string $masterKey
   *   The master key.
   *
   * @return self
   *   The instance.
   */
  public function setMasterKey(string $masterKey): self;

  /**
   * Generates a channel key with globally set master key.
   *
   * @param string $channel
   *   The channel to generate a new key for.
   * @param string $type
   *   The permissions type for the key.
   * @param int $ttl
   *   The time to live for the key. Setting this to 0 will make the key
   *   permanent.
   *
   * @return string
   *   The generated key.
   */
  public function keygenGlobal(string $channel, string $type, int $ttl): string;

}
