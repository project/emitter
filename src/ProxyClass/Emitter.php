<?php
// phpcs:ignoreFile

/**
 * This file was generated via php core/scripts/generate-proxy-class.php 'Drupal\emitter\Emitter' "modules/contrib/emitter/src".
 */

namespace Drupal\emitter\ProxyClass {

    /**
     * Provides a proxy class for \Drupal\emitter\Emitter.
     *
     * @see \Drupal\Component\ProxyBuilder
     */
    class Emitter implements \Drupal\emitter\EmitterInterface
    {

        use \Drupal\Core\DependencyInjection\DependencySerializationTrait;

        /**
         * The id of the original proxied service.
         *
         * @var string
         */
        protected $drupalProxyOriginalServiceId;

        /**
         * The real proxied service, after it was lazy loaded.
         *
         * @var \Drupal\emitter\Emitter
         */
        protected $service;

        /**
         * The service container.
         *
         * @var \Symfony\Component\DependencyInjection\ContainerInterface
         */
        protected $container;

        /**
         * Constructs a ProxyClass Drupal proxy object.
         *
         * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
         *   The container.
         * @param string $drupal_proxy_original_service_id
         *   The service ID of the original service.
         */
        public function __construct(\Symfony\Component\DependencyInjection\ContainerInterface $container, $drupal_proxy_original_service_id)
        {
            $this->container = $container;
            $this->drupalProxyOriginalServiceId = $drupal_proxy_original_service_id;
        }

        /**
         * Lazy loads the real service from the container.
         *
         * @return object
         *   Returns the constructed real service.
         */
        protected function lazyLoadItself()
        {
            if (!isset($this->service)) {
                $this->service = $this->container->get($this->drupalProxyOriginalServiceId);
            }

            return $this->service;
        }

        /**
         * {@inheritdoc}
         */
        public function setMasterKey(string $masterKey): \Drupal\emitter\Emitter
        {
            return $this->lazyLoadItself()->setMasterKey($masterKey);
        }

        /**
         * {@inheritdoc}
         */
        public function keygenGlobal(string $channel, string $type, int $ttl): string
        {
            return $this->lazyLoadItself()->keygenGlobal($channel, $type, $ttl);
        }

        /**
         * {@inheritdoc}
         */
        public function connect(string $host, int $port, ?string $username = NULL): \Wunderwerk\EmitterSDK\Emitter
        {
            return $this->lazyLoadItself()->connect($host, $port, $username);
        }

        /**
         * {@inheritdoc}
         */
        public function disconnect(): \Wunderwerk\EmitterSDK\Emitter
        {
            return $this->lazyLoadItself()->disconnect();
        }

        /**
         * {@inheritdoc}
         */
        public function isConnected(): bool
        {
            return $this->lazyLoadItself()->isConnected();
        }

        /**
         * {@inheritdoc}
         */
        public function addMessageHandler(\Closure $handler): \Wunderwerk\EmitterSDK\Emitter
        {
            return $this->lazyLoadItself()->addMessageHandler($handler);
        }

        /**
         * {@inheritdoc}
         */
        public function addLoopHandler(\Closure $handler): \Wunderwerk\EmitterSDK\Emitter
        {
            return $this->lazyLoadItself()->addLoopHandler($handler);
        }

        /**
         * {@inheritdoc}
         */
        public function removeMessageHandler(\Closure $handler): \Wunderwerk\EmitterSDK\Emitter
        {
            return $this->lazyLoadItself()->removeMessageHandler($handler);
        }

        /**
         * {@inheritdoc}
         */
        public function removeLoopHandler(\Closure $handler): \Wunderwerk\EmitterSDK\Emitter
        {
            return $this->lazyLoadItself()->removeLoopHandler($handler);
        }

        /**
         * {@inheritdoc}
         */
        public function loop(bool $allowSleep = true, bool $exitWhenQueuesEmpty = false, ?int $queueWaitLimit = NULL): \Wunderwerk\EmitterSDK\Emitter
        {
            return $this->lazyLoadItself()->loop($allowSleep, $exitWhenQueuesEmpty, $queueWaitLimit);
        }

        /**
         * {@inheritdoc}
         */
        public function interrupt(): \Wunderwerk\EmitterSDK\Emitter
        {
            return $this->lazyLoadItself()->interrupt();
        }

        /**
         * {@inheritdoc}
         */
        public function publish(string $key, string $channel, string $message, ?int $ttl = NULL, ?bool $me = NULL): \Wunderwerk\EmitterSDK\Emitter
        {
            return $this->lazyLoadItself()->publish($key, $channel, $message, $ttl, $me);
        }

        /**
         * {@inheritdoc}
         */
        public function subscribe(string $key, string $channel, ?int $last = NULL): \Wunderwerk\EmitterSDK\Emitter
        {
            return $this->lazyLoadItself()->subscribe($key, $channel, $last);
        }

        /**
         * {@inheritdoc}
         */
        public function unsubscribe(string $key, string $channel): \Wunderwerk\EmitterSDK\Emitter
        {
            return $this->lazyLoadItself()->unsubscribe($key, $channel);
        }

        /**
         * {@inheritdoc}
         */
        public function publishWithLink(string $link, string $message): \Wunderwerk\EmitterSDK\Emitter
        {
            return $this->lazyLoadItself()->publishWithLink($link, $message);
        }

        /**
         * {@inheritdoc}
         */
        public function link(string $key, string $channel, string $name, bool $private, bool $subscribe, ?int $ttl = NULL, ?bool $me = NULL): \Wunderwerk\EmitterSDK\Emitter
        {
            return $this->lazyLoadItself()->link($key, $channel, $name, $private, $subscribe, $ttl, $me);
        }

        /**
         * {@inheritdoc}
         */
        public function keygen(string $key, string $channel, string $type, int $ttl): string
        {
            return $this->lazyLoadItself()->keygen($key, $channel, $type, $ttl);
        }

        /**
         * {@inheritdoc}
         */
        public function presence(string $key, string $channel, ?bool $status = NULL, ?bool $changes = NULL): \Wunderwerk\EmitterSDK\Emitter
        {
            return $this->lazyLoadItself()->presence($key, $channel, $status, $changes);
        }

        /**
         * {@inheritdoc}
         */
        public function me(): \Wunderwerk\EmitterSDK\Emitter
        {
            return $this->lazyLoadItself()->me();
        }

    }

}
