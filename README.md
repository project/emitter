This module integrates the PHP-SDK for the [emitter event broker](https://emitter.io/).

The module provides a `emitter` service to interact with the emitter server.
Lazy loading is used to only connect to the server when actually necessary.

### Getting started

Setup connection info in settings.php:

```php
$settings['emitter.connection']['host'] = 'localhost';
$settings['emitter.connection']['port'] = 8080;
$settings['emitter.connection']['master_key'] = '__master-key-from-server__';
$settings['emitter.connection']['username']
  = '__username-used-for-connection__';
```

Then use the `emitter` service to interact with the server:


```php
$emitter = \Drupal::service('emitter');

$channel = 'article1/';
$key = $emitter->keygenGlobal($channel, 'rw', 10);

$emitter->publish($key, $channel, 'Hello World!');
```
