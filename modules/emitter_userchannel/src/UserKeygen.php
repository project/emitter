<?php

declare(strict_types=1);

namespace Drupal\emitter_userchannel;

use Drupal\Core\Session\AccountInterface;
use Drupal\emitter\EmitterKeygen;

/**
 * Service to generate user channel key.
 */
class UserKeygen extends EmitterKeygen {

  /**
   * Get key for user channel.
   *
   * Only generates key if not cached already.
   *
   * @param Drupal\Core\Session\AccountInterface $user
   *   The user.
   * @param string $permissions
   *   The permissions.
   *
   * @return string
   *   The key.
   */
  public function getUserKey(AccountInterface $user, string $permissions): string {
    $channel = $this->getChannelName($user->id());

    return $this->getKeyFromCache($channel, $permissions);
  }

  /**
   * Get publish key for all userchannels.
   *
   * This is meant to be only used by the server!!!
   *
   * @return string
   *   The channel key.
   */
  public function getUserChannelPublishKey(): string {
    return $this->getKeyFromCache('user/#/', 'w');
  }

  /**
   * Get user channel name.
   *
   * @param string|int $uid
   *   UID of user.
   *
   * @return string
   *   Generated channel name.
   */
  protected function getChannelName(string|int $uid): string {
    return sprintf('user/%s/#/', $uid);
  }

}
