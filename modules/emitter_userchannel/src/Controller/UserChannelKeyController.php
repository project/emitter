<?php

declare(strict_types=1);

namespace Drupal\emitter_userchannel\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\emitter_userchannel\UserKeygen;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller routines for emitter_userchannel routes.
 */
class UserChannelKeyController extends ControllerBase {

  /**
   * Construct new UserChannelKeyController.
   *
   * @param Drupal\emitter_userchannel\UserKeygen $userKeygen
   *   The user keygen.
   */
  public function __construct(
    private UserKeygen $userKeygen,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('emitter_userchannel.user_keygen'),
    );
  }

  /**
   * Handle GET request.
   *
   * @return Symfony\Component\HttpFoundation\Response
   *   The response.
   */
  public function get(): Response {
    $key = $this->userKeygen->getUserKey(
      $this->currentUser(),
      'rl',
    );

    return new Response($key, Response::HTTP_OK, ['Content-Type' => 'text/plain']);
  }

}
